/*
	HDT API document :
	https://docs.google.com/document/d/1Y2ExHDgSDU7MUEW0TW95Yu84Qoi5YPY4mphxL5WcH34
*/
package main

import (
	"time"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"
	"net/http"
	"net/smtp"
	"io/ioutil"
	"os"
	"bytes"

	log "github.com/Sirupsen/logrus"
)

type Config struct {
	AccessIp 	struct {
		Ip		[]string 	`json:"ip"`
		Updated	string 		`json:"updated"`
	} `json:"access_ip"`
	ShieldIp 	struct {
		Ip		[]string 	`json:"ip"`
		Updated	string 		`json:"updated"`
	} `json:"shield_ip"`
}

type ReqContext struct {
	Key 		string
	Username 	string
	Password 	string
	DateTime 	string
	TransportId	int
	RequestUrl 	string
	ReqMethod	string
	Data		struct {
		IpList 	string
		Action 	string
	}
}

type RespAccessAPI struct {
	Status 		int
	Message		string
	Content 	struct {
		AccessIp 	string
	}
}

// type RespShield struct {
// 	Status 		int
// 	Message		string
// 	Content 	struct {
// 		ShieldIp 	string
// 	}
// }

func initLog() {
	f, err := os.OpenFile("hdt_epsilon.log", os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644)
	Formatter := new(log.TextFormatter)
	Formatter.TimestampFormat = "2006-01-02 15:04:05"
	Formatter.FullTimestamp = true
	log.SetFormatter(Formatter)
	if err != nil {
		// Cannot open log file. Logging to stderr
		fmt.Println(err)
	} else {
		log.SetOutput(f)
	}
}

func initConfig() Config {
    var config Config
    configFile,  err := os.Open("hdt_epsilon.json")
    defer configFile.Close()
    if err != nil {
        fmt.Println(err.Error())
    }
    jsonParser := json.NewDecoder(configFile)
    jsonParser.Decode(&config)
    return config
}

func main() {

	initLog()
	config := initConfig()
	var currentIps []string = config.AccessIp.Ip
	fmt.Println(len(currentIps))

	rc := &ReqContext{
		Key: "7M2P2eR4Qk",
		Username: "Kyungseok",
		TransportId: 8544,
		ReqMethod: "GET",
	}
	rc.RequestUrl = fmt.Sprintf("https://hdt.quantil.com:8082/hdt/transport/%d/access-ip",  rc.TransportId)
	rc.DateTime = strings.Replace(time.Now().UTC().Format(time.RFC1123),  "UTC",  "GMT",  -1)
	rc.Password = getPassword(rc)
	fmt.Println(rc.DateTime)
	fmt.Println(rc.Password)

	respAccess := RespAccessAPI{} // or new(RespAccessAPI)
	requestApi(rc,  &respAccess)
	if respAccess.Status != 1 {
		log.Fatal(fmt.Sprintf("HDT API Error to get IPs : %q", respAccess.Message)
		os.Exit(0)
	}
	// fmt.Println(respAccess.Content.AccessIp)
	var accessIps []string = strings.Split(respAccess.Content.AccessIp,  ",")
	// fmt.Println(accessIps)
	var removed []string = findDiff(currentIps, accessIps)
	var added   []string = findDiff(accessIps, currentIps)
	// fmt.Println(removed)
	// fmt.Println(added)

	if len(added) > 0 { // To be disabled the IP
		log.Info(fmt.Sprintf("Added %d IP(s) : %s", len(added), added))
		// To be calling HDT API to disable the added IP with sending Alert

		rc.ReqMethod = "PUT"
		rc.Data.IpList = strings.Join(added, ",")
		rc.Data.Action = "disable"

		respDisable := RespAccessAPI{} // or new(RespAccessAPI)
		requestApi(rc,  &respDisable)
		if respDisable.Status != 1 {
			log.Fatal(fmt.Sprintf("HDT API Error to disable : %q", respDisable.Message))
			os.Exit(0)
		}
		log.Info(fmt.Sprintf("Disabled %d IP(s) : %q", len(added), strings.Join(added, ",")))

		// Send notification to recipients
		retEmail := sendEmail(added)
		if retEmail != true {
			log.Info(fmt.Sprintf("Sent notification email"))
		} else {
			log.Info(fmt.Sprintf("Couldn't sent notification email"))
		}
	}

	if len(removed) > 0 { // To be updated the IPlist
		log.Info(fmt.Sprintf("Removed %d IP(s) : %s", len(removed), removed))
	}

	if len(added) <= 0 && len(removed) <= 0 {
		log.Info("No action required")
	}

	config.AccessIp.Ip = accessIps
	config.AccessIp.Updated = rc.DateTime

	file, _ := json.MarshalIndent(config, "", " ")
	_ = ioutil.WriteFile("hdt_epsilon.json", file, 0644)
}

func findDiff (a []string,  b []string) []string {
	var diff []string
	for i := 0; i < len(a); i++ {
		if contains(b, a[i]) == false {
			diff = append(diff, a[i])
		}
	}
	return diff
}

func contains(s []string, e string) bool {
    for _, a := range s {
        if a == e {
            return true
        }
    }
    return false
}

func getPassword (c *ReqContext) string {
	key := []byte(c.Key)
	h := hmac.New(sha1.New,  key)
	h.Write([]byte(c.DateTime))
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

func requestApi (c *ReqContext,  t interface{}) error {
	req,  err := http.NewRequest(c.ReqMethod,  c.RequestUrl,  nil)
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth(c.Username,  c.Password)
	req.Header.Set("Date",  c.DateTime)
	req.Header.Set("Content-Type",  "application/json")
	req.Header.Set("Accept",  "application/json")

	trans := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: trans}

	resp,  err := client.Do(req)
	if err != nil{
		log.Fatal(err)
	}
	defer resp.Body.Close()
	return json.NewDecoder(resp.Body).Decode(t)
}

func sendEmail (ipList []string) bool {
	r := strings.NewReplacer("\r\n", "", "\r", "", "\n", "", "%0a", "", "%0d", "")

	from := "CDNetworks US-SRE <us-sd@cdnetworks.com>"
	to := []string{"kyungseok.oh@cdnetworks.com"}
	msg :=  "From: " + from + "\n" +
			"To: " + strings.Join(to, ",") + "\n" +
			"MIME-version: 1.0;\n" +
			"Content-Type: text/html; charset=\"UTF-8\";\n" +
			"Subject: [HDT] Added new VIP in services.citirewards.com\n\n" +
			"<p>\n" +
			"  Dear Support,\n" +
			"  <br><br>\n" +
			"  <b>services.citirewards.com</b> domain have been added " + fmt.Sprintf("%d", len(ipList)) + " new VIP(s).\n" +
			"  <br>\n" +
			"  <br>\n" +
			"  Customer : Epsilon Data Management<br>\n" +
			"  Domain : services.citirewards.com<br>\n" +
			"  Transport ID : 8544<br>\n" +
			"  <br>\n" +
			"  Following VIP(s) disabled automatically by HDT API,<br>\n" +
			"  Disabled IP(s) : <br>\n" +
			"  <br>\n" +
			"  <br>\n" +
			"  Please check above VIP(s) is disabled from <a href=\"https://hdt.quantil.com:8080\" target=\"_blank\">HDT Operation portal</a>\n" +
			"</p>"


	//// Streaming the body:

	// Connect to the remote SMTP server.
	c, err := smtp.Dial("send.mx.cdnetworks.com:25")
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer c.Close()

	// Set the sender and recipients.
	c.Mail(from)
	for i := range to {
		to[i] = r.Replace(to[i])
		if err = c.Rcpt(to[i]); err != nil {
			log.Fatal(err)
			return false
		}
	}

	// Send the email body.
	wc, err := c.Data()
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer wc.Close()

	buf := bytes.NewBufferString(msg)
	if _, err = buf.WriteTo(wc); err != nil {
		log.Fatal(err)
		return false
	}

	log.Info("Sent notification email.")
	return true
}




